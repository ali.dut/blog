<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class IntArrayRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        //REMEMBER: array kontrol işlemi güncellenecek yada gelen int array data model de cast edilecek.

        $newArray = explode(',', str_replace(['{', '}'], '', $value));
        if (!is_array($newArray)) return false;
        foreach ($newArray as $item) {
            if (!is_numeric($item)) return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Veriler int array şeklinde gelmeli!';
    }
}
