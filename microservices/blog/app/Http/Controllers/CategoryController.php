<?php

namespace App\Http\Controllers;

use App\Services\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(CategoryService $service)
    {
        $response = $service->index();

        return response()->json($response);
    }

    public function store(CategoryService $service, Request $request)
    {
        $response = $service->store($request->all());

        return response()->json($response);
    }

    public function search(CategoryService $service, Request $request)
    {
        $response = $service->search($request->all());

        return response()->json($response);
    }

    public function get($id, CategoryService $service)
    {
        $response = $service->get((int)$id);

        return response()->json($response);
    }

    public function update(Request $request, $id, CategoryService $service)
    {
        $response = $service->update($request->all(), (int)$id);

        return response()->json($response);
    }

    public function save(Request $request, CategoryService $service, $id = 0)
    {
        $response = $service->save($request->all(), (int)$id);

        return response()->json($response);
    }

    public function delete($id, CategoryService $service)
    {
        $response = $service->delete((int)$id);

        return response()->json($response);
    }

}
