<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(UserService $service)
    {
        $response = $service->index();

        return response()->json($response);
    }

    public function store(UserService $service, Request $request)
    {
        $response = $service->store($request->all());

        return response()->json($response);
    }

    public function get($id, UserService $service)
    {
        $response = $service->get((int)$id);

        return response()->json($response);
    }

    public function update(Request $request, $id, UserService $service)
    {
        $response = $service->update($request->all(), (int)$id);

        return response()->json($response);
    }

    public function save(Request $request, UserService $service, $id = 0)
    {
        $response = $service->save($request->all(), (int)$id);

        return response()->json($response);
    }

    public function delete($id, UserService $service)
    {
        $response = $service->delete((int)$id);

        return response()->json($response);
    }
}
