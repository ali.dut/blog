<?php

namespace App\Http\Controllers;

use App\Services\BlogService;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index(BlogService $service)
    {
        $response = $service->index();

        return response()->json($response);
    }

    public function store(BlogService $service, Request $request)
    {
        $response = $service->store($request->all());

        return response()->json($response);
    }

    public function search(BlogService $service, Request $request)
    {
        $response = $service->search($request->all());

        return response()->json($response);
    }

    public function get($id, BlogService $service)
    {
        $response = $service->get((int)$id);

        return response()->json($response);
    }

    public function update(Request $request, $id, BlogService $service)
    {
        $response = $service->update($request->all(), (int)$id);

        return response()->json($response);
    }

    public function save(Request $request, BlogService $service, $id = 0)
    {
        $response = $service->save($request->all(), (int)$id);

        return response()->json($response);
    }

    public function delete($id, BlogService $service)
    {
        $response = $service->delete((int)$id);

        return response()->json($response);
    }
}
