<?php

namespace App\Http\Controllers;

use App\Services\AuthService;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login(Request $request, AuthService $service)
    {
        $response = $service->login($request);

        return response()->json($response);
    }

    public function logout(AuthService $service)
    {
        $response = $service->logout();

        return response()->json($response);
    }

}
