<?php

namespace App\Services;

use App\Helpers\ServiceResponse;

class BaseService
{
    /**
     * Serviste olası bir hata durumunda verileri servisreponse'ye göre ayarlıyor.
     * @param $status
     * @param $message
     * @param object|array|null $data
     * @param object|null $e
     * @return ServiceResponse
     */
    public function setResponse($status, $message, object|array|null $data, object|null $e = null): ServiceResponse
    {
        $response = new ServiceResponse();
        if (!empty($e) && !empty($e->_response)) {
            $response->status = $e->_response->status;
            $response->message = $e->_response->message;
            $response->data = $e->_response->data;
            $response->details = $e->_response->details;

            return $response;
        }

        $response->status = $status;
        $response->message = $message;
        $response->data = json_decode(json_encode($data));
        $response->details = [
            'excepion' => empty($e) ? null : array(
                'line' => $e->getLine(),
                'file' => $e->getFile()
            ),
            'service' => empty($e) ? null : array(
                'name' => env('APP_NAME')
            )
        ];

        return $response;
    }
}
