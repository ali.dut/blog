<?php

namespace App\Services;

use App\Classes\Blog;
use App\Helpers\ExceptionHelper;
use App\Helpers\ServiceResponse;
use App\Models\BlogModel;
use App\Queries\BlogQueries;
use App\Rules\IntArrayRule;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;


class BlogService extends BaseService
{
    /**
     * Tüm Blog verilerini çekebilen fonksiyon.
     * @return ServiceResponse
     */
    public function index(): ServiceResponse
    {
        $blog = BlogModel::all();

        return $this->setResponse(200, 'success', ['blog' => $blog->toArray()]);
    }

    /**
     * Yeni blog kaydı için kullanılacak fonksiyon.
     * @param array $data
     * @return ServiceResponse
     */
    public function store(array $data): ServiceResponse
    {
        try {
            \DB::beginTransAction();
            $validator = Validator::make($data, $this->blogRecordRules(), Lang::get('validation'));
            if ($validator->fails()) throw ExceptionHelper::createException($validator->errors(), 406, null);

            $this->validateParams($data);

            $blogCheck = BlogModel::where('title', '=', $data['title'])->first();

            if (!empty($blogCheck)) throw ExceptionHelper::createException('Bu başlık sistemde zaten kayıtlı', 400, null);

            $blog = new Blog();
            $blog->store($data);

            \DB::commit();
            return $this->setResponse(200, 'success', ['blog' => $blog->model->toArray()]);
        } catch (\Exception $e) {
            \DB::rollback();
            return $this->setResponse($e->getCode(), $e->getMessage(), null, $e);
        }
    }

    /**
     * ID verisine göre blog verisi çeken fonksiyon.
     * @param int $id
     * @return ServiceResponse
     */
    public function get(int $id = 0): ServiceResponse
    {
        $blog = BlogModel::find($id);

        return empty($blog) ? $this->setResponse(204, 'Blog not found', null) :
            $this->setResponse(200, 'success', ['blog' => $blog->toArray()]);
    }

    /**
     * Blog verisi içinde arama yapabilmek için kullanılan fonksiyon.
     * @param array $params
     * @return ServiceResponse
     */
    public function search(array $params): ServiceResponse
    {
        try {
            $params = array_merge(BlogQueries::SQL_searchDefaultParams, $params);
            $validator = Validator::make($params, $this->searchParamsRules(), Lang::get('validation'));
            if ($validator->fails()) throw ExceptionHelper::createException($validator->errors(), 406, null);

            $blog = \DB::select(BlogQueries::SQL_search, $params);

            return $this->setResponse(200, 'success', [
                'blog' => $blog
            ]);
        } catch (\Exception $e) {
            return $this->setResponse($e->getCode(), $e->getMessage(), null, $e);
        }

    }

    /**
     * Kayıtlı blogu ID bilgisine göre güncelleyen fonksiyon.
     * @param array $data
     * @param int $id
     * @return ServiceResponse
     */
    public function update(array $data, int $id = 0): ServiceResponse
    {
        try {
            \DB::beginTransAction();
            $validator = Validator::make($data, $this->blogRecordRules(), Lang::get('validation'));
            if ($validator->fails()) throw ExceptionHelper::createException($validator->errors(), 406, null);

            $this->validateParams($data);
            $blog = new Blog($id);

            if (empty($blog->model->id)) {
                $blogCheck = BlogModel::where("title", "=", $data['title'])->first();
                if (empty($blogCheck)) throw ExceptionHelper::createException('Kayıt bulunamadı', 400, null);

                $blog = new Blog($blogCheck['id']);
            }
            $blog->update($data);
            \DB::commit();
            return $this->setResponse(200, 'success', ['blog' => $blog->model->toArray()]);

        } catch (\Exception $e) {
            \DB::rollback();
            return $this->setResponse($e->getCode(), $e->getMessage(), null, $e);
        }
    }

    /**
     * Gönderilen ID varsa güncelleyen yoksa yeni kayıt ekleyen fonksiyon.
     * @param array $data
     * @param int $id
     * @return ServiceResponse
     */
    public function save(array $data, int $id = 0): ServiceResponse
    {
        try {
            \DB::beginTransAction();
            $validator = Validator::make($data, $this->blogRecordRules(), Lang::get('validation'));
            if ($validator->fails()) throw ExceptionHelper::createException($validator->errors(), 406, null);
            $this->validateParams($data);
            $blog = new Blog($id);

            if (empty($blog->model->id)) {
                $BlogModel = BlogModel::where('title', '=', $data['title'])->first();
                if (!empty($BlogModel)) $blog = new Blog($BlogModel->id);
            }

            $blog->save($data);
            \DB::commit();
            return $this->setResponse(200, 'success', ['blog' => $blog->model->toArray()]);
        } catch (\Exception $e) {
            \DB::rollback();
            return $this->setResponse($e->getCode(), $e->getMessage(), null, $e);
        }
    }

    /**
     * Blog tablosundan veri silmek için kullanılan fonksiyon.
     * @param int $id
     * @return ServiceResponse
     */
    public function delete(int $id = 0): ServiceResponse
    {
        try {
            \DB::beginTransAction();
            $count = BlogModel::destroy($id);
            \DB::commit();
            return $this->setResponse(200, 'success', ['count' => $count]);
        } catch (\Exception $e) {
            \DB::rollback();
            return $this->setResponse($e->getCode(), $e->getMessage(), null, $e);
        }
    }

    /**
     * Kayıt için gerekli field'ların kontrolünü sağlayan fonksiyon.
     * @return string[]
     */
    public function blogRecordRules(): array
    {
        return [
            'title' => 'required|string|max:255',
            'content' => 'required|string',
            'category_id' => new IntArrayRule(),
            'details' => 'required|array',
        ];
    }

    /**
     * Search kısmı için gerekli kontrolü yapan fonksiyon.
     * @return string[]
     */
    public function searchParamsRules(): array
    {
        return [
            'search' => 'string|nullable'
        ];
    }

    /**
     * Ekstra kontroller olursa blogservisinde olması gereken toplu olarak burada yapılıp
     * store, update ve save kısımlarına buradan tetiklenecek.
     * @param $data
     * @return void
     */
    public function validateParams($data)
    {
        /*
         * Kontroller yapılacak
         */

    }

}
