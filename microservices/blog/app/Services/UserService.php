<?php

namespace App\Services;

use App\Classes\User;
use App\Helpers\ServiceResponse;
use App\Helpers\ExceptionHelper;
use App\Models\UserModel;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class UserService extends BaseService
{
    /**
     * Tüm kullanıcıları listeleyen fonksiyon.
     * @return ServiceResponse
     */
    public function index(): ServiceResponse
    {
        $users = UserModel::all();

        return $this->setResponse(200, 'success', ['users' => $users->toArray()]);
    }

    /**
     * Yeni kullanıcı ekleyen fonksiyon.
     * @param array $data
     * @return ServiceResponse
     */
    public function store(array $data): ServiceResponse
    {
        try {
            \DB::beginTransAction();
            $validator = Validator::make($data, $this->userRecordRules(), Lang::get('validation'));
            if ($validator->fails()) throw ExceptionHelper::createException($validator->errors(), 406, null);

            $user = UserModel::where('email', '=', $data['email'])->first();
            if (!empty($user)) throw ExceptionHelper::createException('Bu email kaydı daha önce yapılmış.', 406, null);

            // Operasyon
            $user = new User();
            $user->store($data);

            \DB::commit();
            return $this->setResponse(200, 'success', ['user' => $user->model->toArray()]);
        } catch (\Exception $e) {
            \DB::rollback();
            return $this->setResponse($e->getCode(), $e->getMessage(), null, $e);
        }
    }

    /**
     * ID'ye göre kullanıcı getiren fonksiyon.
     * @param int $id
     * @return ServiceResponse
     */
    public function get(int $id = 0): ServiceResponse
    {
        $user = UserModel::find($id);

        return empty($user) ? $this->setResponse(204, 'User not found', null) :
            $this->setResponse(200, 'success', ['user' => $user->toArray()]);
    }

    /**
     * ID'ye göre kullanıcı güncelleyen fonksiyon.
     * @param array $data
     * @param int $id
     * @return ServiceResponse
     */
    public function update(array $data, int $id = 0): ServiceResponse
    {
        try {
            $validator = Validator::make($data, $this->userRecordRules(), Lang::get('validation'));
            if ($validator->fails()) throw ExceptionHelper::createException($validator->errors(), 406, null);

            $user = new User($id);

            if (empty($user->model->id)) throw ExceptionHelper::createException('Identity not found', 204, null);

            $user->update($data);

            return $this->setResponse(200, 'success', ['user' => $user->model->toArray()]);
        } catch (\Exception $e) {
            return $this->setResponse($e->getCode(), $e->getMessage(), null, $e);
        }
    }

    /**
     * Gönderilen ID'de kullanıcı varsa güncelleyen yoksa yenisini oluşturan fonksiyon.
     * @param array $data
     * @param int $id
     * @return ServiceResponse
     */
    public function save(array $data, int $id = 0): ServiceResponse
    {
        try {
            $validator = Validator::make($data, $this->userRecordRules(), Lang::get('validation'));
            if ($validator->fails()) throw ExceptionHelper::createException($validator->errors(), 406, null);

            $user = new User($id);
            $user->save($data);

            return $this->setResponse(200, 'success', ['user' => $user->model->toArray()]);
        } catch (\Exception $e) {
            return $this->setResponse($e->getCode(), $e->getMessage(), null, $e);
        }
    }

    /**
     * ID'ye göre kullanıcı silen fonksiyon.
     * @param int $id
     * @return ServiceResponse
     */
    public function delete(int $id = 0): ServiceResponse
    {
        try {
            $count = UserModel::destroy($id);

            return $this->setResponse(200, 'success', ['count' => $count]);
        } catch (\Exception $e) {
            return $this->setResponse($e->getCode(), $e->getMessage(), null, $e);
        }
    }

    /**
     * Kullanıcı kaydı için gerekli field'leri kontrol eden fonksiyon.
     * @return string[]
     */
    public function userRecordRules(): array
    {
        return [
            'email' => 'required|string|max:255',
            'password' => 'required|string|max:255'
        ];
    }
}
