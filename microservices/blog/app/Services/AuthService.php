<?php

namespace App\Services;

use App\Helpers\ExceptionHelper;
use App\Helpers\ServiceResponse;
use App\Models\UserModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;

class AuthService extends BaseService
{
    /**
     * @param Request $request
     * Kullanıcının email ve password verileri  Request içinde gönderilmelidir.
     * @return \App\Helpers\ServiceResponse
     * Kullanıcının sisteme giriş yapmasını sağlayan fonksiyon.
     */
    public function login(Request $request): ServiceResponse
    {
        $credentials = $request->validate($this->loginRules(), Lang::get('validation'));

        if (\Auth::attempt($credentials)) {
            $user = UserModel::where('email', $credentials['email'])->first();
            $token = $user->createToken('myapptoken')->plainTextToken;
            $data = [
                'user' => $user,
                'token' => $token
            ];

            return $this->setResponse(200, 'success', $data);
        } else {
            return $this->setResponse(401, 'Kullanıcı adı veya şifre hatalı!', null);
        }
    }

    /**
     * @return \App\Helpers\ServiceResponse
     * Kullanıcının sistemden çıkış yapmasını sağlayan fonksiyon.
     */
    public function logout(): ServiceResponse
    {
        auth()->user()->tokens()->delete();

        return $this->setResponse(200, 'success', null);
    }

    /**
     * Login için gerekli field'lerin kontrolü için gerekli fonksiyon.
     * @return string[]
     */
    public function loginRules(): array
    {
        return [
            'email' => 'required|string',
            'password' => 'required|string'
        ];
    }
}
