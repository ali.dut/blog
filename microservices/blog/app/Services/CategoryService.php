<?php

namespace App\Services;

use App\Classes\Category;
use App\Helpers\ExceptionHelper;
use App\Helpers\ServiceResponse;
use App\Models\CategoryModel;
use App\Queries\CategoryQueries;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;


class CategoryService extends BaseService
{
    /**
     * Tüm kategorileri listeleyen fonksiyon.
     * @return ServiceResponse
     */
    public function index(): ServiceResponse
    {
        $category = CategoryModel::all();

        return $this->setResponse(200, 'success', ['category' => $category->toArray()]);
    }

    /**
     * Yeni kategori eklemek için kullanılan fonksiyon.
     * @param array $data
     * @return ServiceResponse
     */
    public function store(array $data): ServiceResponse
    {
        try {
            \DB::beginTransAction();
            $validator = Validator::make($data, $this->categoryRecordRules(), Lang::get('validation'));
            if ($validator->fails()) throw ExceptionHelper::createException($validator->errors(), 406, null);

            $this->validateParams($data);

            $categoryCheck = CategoryModel::where('name', '=', $data['name'])->first();

            if (!empty($categoryCheck)) throw ExceptionHelper::createException('Bu kategori sistemde zaten kayıtlı', 400, null);

            $category = new Category();
            $category->store($data);

            \DB::commit();
            return $this->setResponse(200, 'success', ['category' => $category->model->toArray()]);
        } catch (\Exception $e) {
            \DB::rollback();
            return $this->setResponse($e->getCode(), $e->getMessage(), null, $e);
        }
    }

    /**
     * Tek bir kategori çekmek için kullanılan fonksiyon.
     * @param int $id
     * @return ServiceResponse
     */
    public function get(int $id = 0): ServiceResponse
    {
        $category = CategoryModel::find($id);

        return empty($category) ? $this->setResponse(204, 'Category not found', null) :
            $this->setResponse(200, 'success', ['category' => $category->toArray()]);
    }

    /**
     * Kategori tablosunda arama yapabilmek için kullanılan fonksiyon.
     * @param array $params
     * @return ServiceResponse
     */
    public function search(array $params): ServiceResponse
    {
        try {
            $params = array_merge(CategoryQueries::SQL_searchDefaultParams, $params);
            $validator = Validator::make($params, $this->searchParamsRules(), Lang::get('validation'));
            if ($validator->fails()) throw ExceptionHelper::createException($validator->errors(), 406, null);

            $category = \DB::select(CategoryQueries::SQL_search, $params);

            return $this->setResponse(200, 'success', [
                'category' => $category
            ]);
        } catch (\Exception $e) {
            return $this->setResponse($e->getCode(), $e->getMessage(), null, $e);
        }

    }

    /**
     * Kategori tablosunda güncelleme yapabilmek için kullanılan fonksiyon.
     * @param array $data
     * @param int $id
     * @return ServiceResponse
     */
    public function update(array $data, int $id = 0): ServiceResponse
    {
        try {
            \DB::beginTransAction();
            $validator = Validator::make($data, $this->categoryRecordRules(), Lang::get('validation'));
            if ($validator->fails()) throw ExceptionHelper::createException($validator->errors(), 406, null);

            $this->validateParams($data);
            $category = new Category($id);

            if (empty($category->model->id)) {
                $categoryCheck = CategoryModel::where("name", "=", $data['name'])->first();
                if (empty($categoryCheck)) throw ExceptionHelper::createException('Kayıt bulunamadı', 400, null);

                $category = new Category($categoryCheck['id']);
            }
            $category->update($data);
            \DB::commit();
            return $this->setResponse(200, 'success', ['category' => $category->model->toArray()]);

        } catch (\Exception $e) {
            \DB::rollback();
            return $this->setResponse($e->getCode(), $e->getMessage(), null, $e);
        }
    }

    /**
     * Kategori tablosuna ID varsa güncelleme yoksa yeni ekleme işlemi yapan fonksiyon.
     * @param array $data
     * @param int $id
     * @return ServiceResponse
     */
    public function save(array $data, int $id = 0): ServiceResponse
    {
        try {
            \DB::beginTransAction();
            $validator = Validator::make($data, $this->categoryRecordRules(), Lang::get('validation'));
            if ($validator->fails()) throw ExceptionHelper::createException($validator->errors(), 406, null);
            $this->validateParams($data);
            $category = new Category($id);

            if (empty($category->model->id)) {
                $CategoryModel = CategoryModel::where('name', '=', $data['name'])->first();
                if (!empty($CategoryModel)) $category = new Category($CategoryModel->id);
            }

            $category->save($data);
            \DB::commit();
            return $this->setResponse(200, 'success', ['category' => $category->model->toArray()]);
        } catch (\Exception $e) {
            \DB::rollback();
            return $this->setResponse($e->getCode(), $e->getMessage(), null, $e);
        }
    }

    /**
     * Kategori tablosundan veri silen fonksiyon.
     * @param int $id
     * @return ServiceResponse
     */
    public function delete(int $id = 0): ServiceResponse
    {
        try {
            \DB::beginTransAction();
            $count = CategoryModel::destroy($id);
            \DB::commit();
            return $this->setResponse(200, 'success', ['count' => $count]);
        } catch (\Exception $e) {
            \DB::rollback();
            return $this->setResponse($e->getCode(), $e->getMessage(), null, $e);
        }
    }

    /**
     * Kategori tablosunun field kontrolünü yapan fonksiyon.
     * @return string[]
     */
    public function categoryRecordRules(): array
    {
        return [
            'name' => 'required|string|max:255'
        ];
    }

    /**
     * Arama kısmının kontrolünü yapan fonksiyon.
     * @return string[]
     */
    public function searchParamsRules(): array
    {
        return [
            'search' => 'string|nullable'
        ];
    }

    /**
     * CategoryService kısmında ekstra bir validate işlemi gerekirse burada yapılıp
     * store, update ve save fonksionlarına eklenecek.
     * @param $data
     * @return void
     */
    public function validateParams($data)
    {
        /*
         * Kontroller yapılacak
         */

    }

}
