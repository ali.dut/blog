<?php

namespace App\Queries;

class BlogQueries
{
    //TODO: Ön yüz bittikten sonra buranın kontrolü yapılacak.
    const SQL_search = <<<SQL
    SELECT
            DISTINCT
            b.id as id,
            b.title as title,
            b.content as content,
            b.details as details,
            "array_agg"(c."name")  over w as category
    FROM
            public.tb_blog b
        JOIN
            public.tb_category c
        ON c.id = ANY(b.category_id)
    WHERE
        (
            ( '' = :search OR :search IS NULL) OR (concat( c.name, b.title, b.content ) like '%' || :search || '%')
        )
    WINDOW w AS( PARTITION BY b.id )
    SQL;

    const SQL_searchDefaultParams = [
        "search" => "",
    ];

}
