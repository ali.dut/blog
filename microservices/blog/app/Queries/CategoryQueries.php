<?php

namespace App\Queries;

class CategoryQueries
{
    const SQL_search = <<<SQL
    SELECT
        c.id as id,
        c.name as name
    FROM
            public.tb_category c
    WHERE
        (
            ( '' = :search OR :search IS NULL) OR (c.name like '%' || :search || '%')
        )
    SQL;

    const SQL_searchDefaultParams = [
        "search" => "",
    ];

}
