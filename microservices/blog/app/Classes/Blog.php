<?php

namespace App\Classes;

use App\Models\BlogModel;

class Blog
{
    public $model;

    /**
     * ID varsa kayıtlı modeli getiriyor yoksa yeni model oluşturuyor.
     * @param int $id ,
     */
    public function __construct(int $id = 0)
    {
        $this->model = empty($id) ? null : BlogModel::find($id);
        $this->model = empty($this->model) ? new BlogModel() : $this->model;
    }

    /**
     * Eğer id varsa güncelleme yapıyor yoksa yeni kayıt işlemini gerçekleştiriyor.
     * @param array|null $data ,
     * @param int $id ,
     * @return object
     * @throws \Exception
     */
    public function save(array $data = null, int $id = 0)
    {
        if (empty($this->model->id)) return $this->store($data);

        return $this->update($data);
    }

    /**
     * id varsa güncelleme yapılacak olan güncelleme işlemi
     * @param array|null $data
     * @return object
     * @throws \Exception
     */
    public function update(array $data = null)
    {
        $this->setModel($data);
        $this->model->update();

        return $this->model;
    }

    /**
     * @param array|null $data
     * @return object
     * @throws \Exception
     */
    public function store(array $data = null)
    {
        $this->setModel($data);
        $this->model->save();

        return $this->model;
    }

    /**
     * Verilerin modele işlendiği bölüm
     */
    public function setModel(array $data = null)
    {
        $this->model->title = $data['title'];
        $this->model->content = $data['content'];
        $this->model->category_id = $data['category_id'];
        $this->model->details = $data['details'];
    }
}
