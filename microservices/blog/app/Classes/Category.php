<?php

namespace App\Classes;

use App\Models\CategoryModel;

class Category
{
    public $model;
    /**
     * ID varsa kayıtlı modeli getiriyor yoksa yeni model oluşturuyor.
     * @param int $id,
     */
    public function __construct(int $id = 0)
    {
        $this->model = empty($id) ? null : CategoryModel::find($id);
        $this->model = empty($this->model) ? new CategoryModel() : $this->model;
    }
    /**
     * Eğer id varsa güncelleme yapıyor yoksa yeni kayıt işlemini gerçekleştiriyor.
     * @param array|null $data,
     * @param int $id,
     * @return object
     * @throws \Exception
     */
    public function save(array $data = null, int $id = 0)
    {
        if (empty($this->model->id)) return $this->store($data);

        return $this->update($data);
    }
    /**
     * id varsa güncelleme yapılacak olan güncelleme işlemi
     * @param array|null $data
     * @return object
     * @throws \Exception
     */
    public function update(array $data = null)
    {
        $this->setModel($data);
        $this->model->update();

        return $this->model;
    }
    /**
     * @param array|null $data
     * @return object
     * @throws \Exception
     */
    public function store(array $data = null)
    {
        $this->setModel($data);
        $this->model->save();

        return $this->model;
    }
    /**
     * Verilerin modele işlendiği bölüm
     */
    public function setModel(array $data = null)
    {
        $this->model->name = $data['name'];
    }
}
