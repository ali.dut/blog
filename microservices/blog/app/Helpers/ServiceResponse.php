<?php

namespace App\Helpers;
/**
 * Servislerin hepsinin tek dönüş tipi olması ve api'yi kullanacak kullanıcıların belirli bir düzende veriye erişmeleri için belirlenen format
 */
class ServiceResponse
{
    public $status;
    public $message;
    public $data;
    public $details;
}
