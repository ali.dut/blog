<?php

namespace App\Helpers;


class ExceptionHelper
{
    /**
     * Daha önce çalıştığım projede mikroservislerin hangisinde hata olduğunu görebilmek için bu dönüş şeklini tüm ms'lerde kullanıyorduk.
     * message ve status bu servisin hatasını gösteriyor. Response ise önceki servisten gelen mesajı gösteriyor.
     * Bu proje monolitik yapıda olduğu için böyle bir şey olmayacak ama aynı formatta ilerlemek istedim.
     * @param string|null $message
     * @param string|null $status
     * @param string|null $response
     * @return object
     * @throws \Exception
     */
    public static function createException($message, $status, $response)
    {
        $exception = new \Exception($message, $status);
        $exception->_response = $response;

        return $exception;
    }
}
