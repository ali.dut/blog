<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlogModel extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';
    protected $table = 'public.tb_blog';
    protected $dateFormat = 'U';
    public $timestamps = false;
    public bool $afterCommit = true;
    protected $attributes = [];
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'title',
        'content',
        'category_id',
        'details',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'title' => 'string',
        'content' => 'string',
        'details' => 'array',
    ];
}
