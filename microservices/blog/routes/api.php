<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Auth Kısmını kontrol eden bölüm.
 */
Route::post('/login', [AuthController::class, 'login'])->withoutMiddleware(['auth:sanctum', 'api']);
Route::get('/logout', [AuthController::class, 'logout']);

/**
 * Giriş yapmamışsa yönlendirilecek bölüm.
 */
Route::get('/unauthorised', function () {
    $response = new stdClass();
    $response->status = 401;
    $response->message = 'Please login';
    $response->data = null;
    $response->details = [
        'excepion' => null,
        'service' => array(
            'name' => env('APP_NAME')
        )
    ];

    return Illuminate\Support\Facades\Response::json($response);
})->withoutMiddleware(['auth:sanctum', 'api'])->name('login');


/**
 * User kısmını kontrol eden bölüm.
 */
Route::prefix('/user')->group(function () {
    Route::get('/', [UserController::class, 'index']);
    Route::post('/', [UserController::class, 'store']);
    Route::get('/{id}', [UserController::class, 'get']);
    Route::put('/{id}', [UserController::class, 'update']);
    Route::patch('/{id}', [UserController::class, 'save']);
    Route::delete('/{id}', [UserController::class, 'delete']);
});

/**
 * Category kısmını kontrol eden bölüm.
 */
Route::prefix('/category')->group(function () {
    Route::get('/', [CategoryController::class, 'index']);
    Route::post('/', [CategoryController::class, 'store']);
    Route::post('/search', [CategoryController::class, 'search'])->withoutMiddleware(['auth:sanctum', 'api']);
    Route::get('/{id}', [CategoryController::class, 'get']);
    Route::put('/{id}', [CategoryController::class, 'update']);
    Route::patch('/{id}', [CategoryController::class, 'save']);
    Route::delete('/{id}', [CategoryController::class, 'delete']);
});

/**
 * Blog kısmını kontrol eden bölüm.
 */
Route::prefix('/blog')->group(function () {
    Route::get('/', [BlogController::class, 'index']);
    Route::post('/', [BlogController::class, 'store']);
    Route::post('/search', [BlogController::class, 'search'])->withoutMiddleware(['auth:sanctum', 'api']);
    Route::get('/{id}', [BlogController::class, 'get']);
    Route::put('/{id}', [BlogController::class, 'update']);
    Route::patch('/{id}', [BlogController::class, 'save']);
    Route::delete('/{id}', [BlogController::class, 'delete']);
});



