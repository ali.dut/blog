# Devops

## DOCKER

INSTALLATION COMMANDS

STEP-1
Start the container in detached mode:

```sh
docker-compose up -d
```

STEP-2
Composer Install:

```sh
docker exec blog-* composer install
```

OPTIONAL COMMANDS

Database Backup:
```
docker exec blog-db /usr/bin/pg_dump --verbose --username=postgres --clean --no-password --no-owner --format=c -Z9 --file=/tmp/backup/db.dump.gz db_blog
```

Database Restore:
```
docker exec blog-db /usr/bin/pg_restore -v --jobs=8 --no-owner --username=postgres --no-acl -d db_blog /tmp/backup/db.dump.gz 2>&1
```

Tail logs (example):

```sh
docker logs blog-nginx --follow
```

Connect container bash ( sh for alpine):
```sh
docker exec -it blog-nginx bash
```

Bring down the container:
```sh
docker stop blog-nginx
```

## PROJECT CONFIGURATION
CRON JOBS

STEP-1
Open crontab conf:
```sh
crontab -e
```